import 'package:flutter/material.dart';

class MaintenancePage extends StatefulWidget {
  // const MaintenancePage({Key? key}) : super(key: key);
  static const String routeName = '/maintenance';
  final Text apptext = new Text("Maintenance");

  @override
  _MaintenancePageState createState() => _MaintenancePageState();
}

class _MaintenancePageState extends State<MaintenancePage> {
  @override
  Widget build(BuildContext context) {
    return Container(child: Padding(
      padding: EdgeInsets.all(2),
      child: ListView.separated(
        padding: EdgeInsets.all(8),
        itemCount: listMaintenance.length,
        // reverse: true,
        separatorBuilder: (buildContext, index) {
          // return Container(
          //   color: Colors.red,
          //   height: 2,
          // );
          // return Text("Pemisah");
          if (index == 4)
            return Container(
              color: Colors.red[100],
              padding: EdgeInsets.all(8),
              child: Center(
                child: Text("Space Iklan"),
              ),
            );

          // return Text("Kosong");
          return Container();
        },
        itemBuilder: (buildContext, index) {
          var today = new DateTime.now();
          var viewdate = today.add(new Duration(days: index+1));

          return ListTile(
            leading: Icon(Icons.access_alarm),
            title: Text(listMaintenance[index]),
            subtitle: Text("Tanggal: ${viewdate}"),
            trailing: Icon(Icons.arrow_forward_ios_rounded),
            onTap: () {
              print("klik on tap");
            },
            onLongPress: () => print("on long press"),
          );
        },
      ),
    ),);
  }

  var listMaintenance = [
    "Ganti Air",
    "Cek Kadar Air",
    "Dosing Vitamin AB+",
    "Cek Phospate",
    "Cek kadar Nitrat"
  ];
}
