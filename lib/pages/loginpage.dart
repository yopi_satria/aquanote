import 'dart:convert';

import 'package:aquanote/layout/contentpage.dart';
import 'package:aquanote/main.dart';
import 'package:aquanote/model/logindata.dart';
import 'package:aquanote/model/mlogin.dart';

// import 'package:aquanote/pages/homepage.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'dashboardpage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  MultiState? ms;

  @override
  Widget build(BuildContext context) {
    ms = Provider.of<MultiState>(context);
    GetStorage storage = GetStorage();
    String? token = storage.read("token");

    print(token);

    return (token == null ? LoginScreen() :
        ContentPage(
      apptext: DashboardPage().apptext,
      child: DashboardPage().createState().build(context),
    ));
  }
}
class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  MultiState? multistate;
  @override
  Widget build(BuildContext context){
    multistate = Provider.of<MultiState>(context);
    return ChangeNotifierProvider(
      create: (_) => KontenState(this.multistate, context),
      child: Consumer<KontenState>(builder: (context, state, _){
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 300,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/headerbg.png'),
                            fit: BoxFit.fill
                        )
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                          left: 30,
                          width: 100,
                          height: 250,
                          child: AnimatedOpacity(opacity: 1.0,
                              duration: Duration(milliseconds: 600),
                              child: Image(
                                  image: AssetImage('assets/images/fish.png')
                              )
                          ),
                        ),
                        // TweenAnimationBuilder(
                        //     tween: _scaleTween,
                        //     duration: Duration(milliseconds: 600),
                        //     builder: (context,scale,child){
                        //       return Transform.scale(scale: scale as double,child: child);
                        //     },
                        //     child: Positioned(
                        //         left: 30,
                        //         width: 100,
                        //         height: 250,
                        //         child: Image(
                        //
                        //             image: AssetImage('assets/images/fish.png')
                        //         )),
                        // ),
                        Positioned(
                          left: 300,
                          width: 100,
                          height: 250,
                          child: Image(
                              image: AssetImage('assets/images/fish.png')
                          ),

                        ),
                        Positioned(
                          left: 20,
                          top: 240,
                          width: 400,
                          child: Text(
                            'Aquarium Diary',
                            style: TextStyle(fontFamily: 'FontHeader',color: Colors.blue),
                            textScaleFactor: 2,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 0, 30.0, 0 ),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(3.0),
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(
                                color: Colors.grey
                            )),
                          ),
                          child: TextFormField(
                            controller: state.uname,
                            validator: (value){
                              if(value == null || value.isEmpty){
                                return "field username harus diisi";
                              }
                            },
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Username",
                                hintStyle: TextStyle(color: Colors.grey[400])
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(3.0),
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(
                                color: Colors.grey
                            )),
                          ),
                          child: TextFormField(
                            controller: state.pass,
                            validator: (value){
                              if(value == null || value.isEmpty){
                                return "field password harus diisi";
                              }
                            },
                            obscureText: true,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Password",
                                hintStyle: TextStyle(color: Colors.grey[400])
                            ),
                          ),
                        ),
                        if(state.warning != null) Text("${state.warning}"),
                        Container(
                          width: 300,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0,30.0,0,0),
                            child: ElevatedButton(
                              onPressed: () {
                                state.prosesLogin();
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(builder: (context) => Homepage()),
                                // );
                              },
                              child: Text('LOGIN'),
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12), // <-- Radius
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        }
      ),
    );
  }
}

class KontenState extends ChangeNotifier{
  SharedPreferences? pref;
  MultiState? mstate;
  TextEditingController uname = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  List<String> data =[];
  String? warning;
  BuildContext ctx;
  LoginData? logindata;

  KontenState(this.mstate, this.ctx){
    // if(this.mstate!.token != null){
    //   getData();
    // }
    // print("$data");
  }

  // void getData() {
  //   if (data.length >0){
  //     data.clear();
  //     this.data.add("admin");
  //     this.data.add("administrator");
  //     notifyListeners();
  //   }
  // }

  // getDetaildata(){
  //   print(this.warning);
  //   if (data.length >0){
  //     this.data.add("admin");
  //     this.data.add("administrator");
  //     this.data.add("admin@email.com");
  //     this.data.add("Role SuperUser");
  //     notifyListeners();
  //   }
  // }

  // prosesLoginProvider() async {
  //   //print("${this.uname.text}  and ${this.pass.text}");
  //   if (this.uname.text != "admin"){
  //       this.warning = "username salah";
  //   }else if (this.pass.text != "123"){
  //       this.warning = "password salah";
  //   }else{
  //     mstate!.setToken("123456789");
  //     pref = await SharedPreferences.getInstance();
  //     pref?.setString("token", "Bearer 123456789");
  //     //Navigator.pop(ctx, Homepage());
  //     //print(pref!.getString("token"));
  //
  //     GetStorage prefstorage = GetStorage();
  //     prefstorage.write("token",
  //         "Bearer ${DateTime.now().millisecondsSinceEpoch}");
  //
  //     Navigator.pushReplacementNamed(ctx, "/dashboard");
  //     // Navigator.pop(
  //     //     ctx,
  //     //     MaterialPageRoute(builder: (context) => Homepage()),
  //     //   );
  //   }
  //   notifyListeners();
  // }

  prosesLogin() async{
    // this.muncul = false;
    // this.warning = "";
    logindata = null;
    print("proses login");
    if (this.uname.text == ""){
      this.warning = "username salah";
    }else if (this.pass.text == ""){
      this.warning = "password salah";
    }else {
      //notifyListeners();
      Uri url = Uri.parse("https://sfa.forcapos.xyz/api/account/login");
      var header;
      var body = {
        "m_akun_namapengguna": uname.text,
        "m_akun_password": pass.text
      };
      print(body);
      http.post(url, headers: header, body: body).then((value) {
        //print(value.body);
        try {
          if (value.body.isNotEmpty) {
            var json = jsonDecode(value.body);
            Mlogin dataStatus = Mlogin.fromJson(json);
            if (dataStatus.status!.toUpperCase() == "OK") {
              if (dataStatus.data!.length > 0) {
                logindata = dataStatus.data!.first;
                print(logindata!.mToken);

                //simpan persistence
                GetStorage prefstorage = GetStorage();
                prefstorage.write("token", "Bearer ${logindata!.mToken}");
                prefstorage.write("akun", logindata!.mAkunNamapengguna);
                //navigasi ke dashboard
                Navigator.pushReplacementNamed(ctx, "/dashboard");
                //this.muncul = true;
              } else {
                this.warning = "Data kosong";
              }
            } else {
              this.warning = dataStatus.message;
            }
          } else {
            this.warning = "Data tidak ada";
          }
        } catch (e) {
          this.warning = "http failed";
        }

      });
    }
    notifyListeners();
  }
}
