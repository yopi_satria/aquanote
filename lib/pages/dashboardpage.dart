import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class DashboardPage extends StatefulWidget {
  // const DasboardPage({Key? key}) : super(key: key);
  static const String routeName = '/dashboard';
  final Text apptext = new Text("Dashboard");

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  void deactivate() {
    print("DashboardRoute: deactivate");
    super.deactivate();
  }

  @override
  void dispose() {
    print("DashboardRoute: dispose");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // SharedPreferences? pref;
    // String? token;

    // void _getToken() async {
    //   SharedPreferences prefs = await SharedPreferences.getInstance();
    //   setState(() {
    //     token = prefs.getString("token");
    //   });
    // }
    GetStorage prefs = GetStorage();
    print("token ${prefs.read("token")}");
    // token = prefs.read("token");

    return Container(
      child: Column(
        children: [
          Padding(
              padding: EdgeInsets.all(20),
              child: Text("DASHBOARD",
                  style: TextStyle(fontFamily: 'FontHeader',
                      fontSize: 40,
                      color: Colors.blue))
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child:
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          color: Colors.red,
                        ),
                        margin: EdgeInsets.all(8),
                        height: 140,
                        width: 140,
                        child: FittedBox(
                          fit: BoxFit.contain,
                          child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Image(
                                        height: 100,
                                        width: 100,
                                        image: AssetImage('assets/images/aquarium.png')
                                    ),
                                    Column(
                                      children: [
                                        Text("Aquarium", style: TextStyle(fontSize: 30, color: Colors.white),),
                                        Text("Total", style: TextStyle(fontSize: 15, color: Colors.white),),
                                        Text("4", style: TextStyle(fontSize: 40, color: Colors.white),),
                                      ],
                                    )

                                  ],
                                ),
                              )),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child:
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          color: Colors.green,
                        ),
                        margin: EdgeInsets.all(8),
                        height: 140,
                        width: 140,
                        child: FittedBox(
                          fit : BoxFit.contain,
                          child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Image(
                                        height: 120,
                                        width: 120,
                                        image: AssetImage('assets/images/fish2.png')
                                    ),
                                    Column(
                                      children: [
                                        Text(" Biota ", style: TextStyle(fontSize: 30, color: Colors.white),),
                                        Text("Total", style: TextStyle(fontSize: 15, color: Colors.white),),
                                        Text("20", style: TextStyle(fontSize: 40, color: Colors.white),),
                                      ],
                                    )

                                  ],
                                ),
                              )),
                        ),
                      ),

                    ),
                    Expanded(
                      flex: 3,
                      child:
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          color: Colors.red,
                        ),
                        margin: EdgeInsets.all(8),
                        height: 140,
                        width: 140,
                        child: FittedBox(
                          fit: BoxFit.contain,
                          child: Center(
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(Icons.article, color: Colors.white,size: 120,),
                                    Column(
                                      children: [
                                        Text("Maintenance", style: TextStyle(fontSize: 30, color: Colors.white),),
                                        Text("Total", style: TextStyle(fontSize: 15, color: Colors.white),),
                                        Text("50", style: TextStyle(fontSize: 40, color: Colors.white),),
                                      ],
                                    )
                                  ],
                                ),
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),

        ],
      ),
    );
  }
}
