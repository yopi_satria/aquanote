import 'package:aquanote/layout/customwidget.dart';
import 'package:aquanote/model/mnews.dart';
import 'package:aquanote/model/newsdata.dart';
import 'package:flutter/material.dart';
// import 'package:shimmer/shimmer.dart';

class NewsFeedPage extends StatefulWidget {

  // const NewsFeedPage({Key? key}) : super(key: key);
  static const String routeName = '/newsfeed';
  final Text apptext = new Text("News Feed");

  @override
  _NewsFeedPageState createState() => _NewsFeedPageState();
}

class _NewsFeedPageState extends State<NewsFeedPage> {
  List<NewsModel> news = [];
  bool isLoading = true;

  @override
  void initState() {
    print("inital class");
    loadData();
    super.initState();
  }

  Future loadData() async {
    setState(() {
      isLoading = true;
    });
    await Future.delayed(Duration(seconds: 5));
    news = List.of(allNews);
     setState(() {
      isLoading = false;
     });
  }


  @override
  Widget build(BuildContext context) {
    print("build news");
    print(isLoading);

    return Container(child: Center(
      child: ListView.builder(
            itemCount: isLoading? 5: news.length,
            itemBuilder: (context, index) {
              if (isLoading) {
                return buildNewsShimmer();
              } else {
                final newsitem = news[index];
                return buildNewsList(newsitem);
              }
            }
        ),
    ),
    );
  }

  Widget buildNewsList(NewsModel model) =>
      ListTile(
        leading: CircleAvatar(
          radius: 30,
          backgroundImage: NetworkImage(model.urlImg),
        ),
        title: Text(model.title, style: TextStyle(fontSize: 16),),
        subtitle: Text(
          model.detail, style: TextStyle(fontSize: 14), maxLines: 1,),
      );

  Widget buildNewsShimmer() =>
      ListTile(
        leading: CustomWidget.rectangular(height: 64, width: 64),
        title: Align(
          alignment: Alignment.centerLeft,
          child: CustomWidget.rectangular(height: 16,
            width: 300,),
        ),
        subtitle: CustomWidget.rectangular(height: 14),
      );

}
