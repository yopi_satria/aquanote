// import 'package:aquanote/exam/ContohPersistence.dart';
import 'package:aquanote/pages/biotapage.dart';
import 'package:aquanote/pages/dashboardpage.dart';
import 'package:aquanote/pages/maintenancepage.dart';
import 'package:aquanote/pages/newsfeedpage.dart';
import 'package:aquanote/pages/tankpage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'layout/contentpage.dart';
import 'pages/loginpage.dart';
// import 'pages/homepage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MultiState? ms;
  SharedPreferences? pref;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (c) => MultiState()),
      ],
      child: MaterialApp(
        title: 'Flutter Aquarium Note',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        // home: ContohPersistence(),

        // MyHomePage(title: 'Flutter Demo Home Page'),
        initialRoute: "/",
        routes: {
          '/': (context) => LoginPage(),
          '/dashboard': (context) => ContentPage(
                apptext: DashboardPage().apptext,
                child: DashboardPage().createState().build(context),
              ),
          '/tank': (context) => ContentPage(
                apptext: TankPage().apptext,
                child: TankPage().createState().build(context),
              ),
          '/biota': (context) => ContentPage(
                apptext: BiotaPage().apptext,
                child: BiotaPage().createState().build(context),
              ),
          '/maintenance': (context) => ContentPage(
                apptext: MaintenancePage().apptext,
                child: MaintenancePage().createState().build(context),
              ),
          '/newsfeed': (context) => ContentPage(
                apptext: NewsFeedPage().apptext,
                child: NewsFeedPage().createState().build(context),
              ),
        },
      ),
    );
  }
}

class MultiState extends ChangeNotifier {
  String? token;

  setToken(String? vtoken) {
    this.token = vtoken;
    notifyListeners();
  }
}
