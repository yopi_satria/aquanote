class NewsModel {
  final String urlImg;
  final String title;
  final String detail;


  const NewsModel({required this.urlImg,
    required this.title,
    required this.detail});
}