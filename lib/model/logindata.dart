class LoginData {
  int? mAkunId;
  String? mAkunNamapengguna;
  String? mAkunEmail;
  String? mAkunLinkPhoto;
  String? mToken;

  LoginData(
      {this.mAkunId,
        this.mAkunNamapengguna,
        this.mAkunEmail,
        this.mAkunLinkPhoto,
        this.mToken});

  LoginData.fromJson(dynamic json) {
    mAkunId = json["m_akun_id"];
    mAkunNamapengguna = json["m_akun_namapengguna"];
    mAkunEmail = json["m_akun_email"];
    mAkunLinkPhoto = json["m_akun_link_photo"];
    mToken = json["m_token"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["m_akun_id"] = mAkunId;
    map["m_akun_namapengguna"] = mAkunNamapengguna;
    map["m_akun_email"] = mAkunEmail;
    map["m_akun_link_photo"] = mAkunLinkPhoto;
    map["m_token"] = mToken;
    return map;
  }
}
