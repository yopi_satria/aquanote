import 'package:flutter/material.dart';
import 'package:aquanote/layout/pageroutes.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int _selectedindex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.article),
            label: "Todo List",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.all_inbox_sharp),
            label: "News Feed",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "Profile",
          ),
        ],
        currentIndex: _selectedindex,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.blue,

        onTap: (int index){
          setState(() {
            _selectedindex = index;
            if(index == 2)
              Navigator.pushReplacementNamed(context, pageRoutes.newsfeed);
          });
        },
      ),
    );
  }
}
