import 'package:aquanote/pages/biotapage.dart';
import 'package:aquanote/pages/dashboardpage.dart';
import 'package:aquanote/pages/newsfeedpage.dart';
import 'package:aquanote/pages/tankpage.dart';

mixin pageRoutes {
  static const String dashboard = DashboardPage.routeName;
  static const String tank = TankPage.routeName;
  static const String biota = BiotaPage.routeName;
  static const String maintenance = '/maintenance';

  //bottom Route
  static const String newsfeed = NewsFeedPage.routeName;
  static const String todolist = '/todolist';
  static const String profile = '/profile';

}