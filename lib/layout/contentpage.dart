import 'package:flutter/material.dart';
import 'package:aquanote/layout/navigationdrawer.dart';

import 'bottomnav.dart';

class ContentPage extends StatelessWidget {
  final Widget? child;
  final Text? apptext;

  const ContentPage({
    Key? key,
    this.apptext,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: apptext,
      ),
      drawer: NavigationDrawer(),
      body: Center(child: child),
      bottomNavigationBar: BottomNav(),
    );
  }

// @override
// _ContentPageState createState() => _ContentPageState();
}