// import 'package:aquanote/pages/loginpage.dart';
import 'package:flutter/material.dart';
import 'package:aquanote/layout/pageroutes.dart';
import 'package:get_storage/get_storage.dart';

class NavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(),
          createDrawerBodyItem(
            icon: Icons.home,
            text: 'Dashboard',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.dashboard),
          ),
          createDrawerBodyItem(
            icon: Icons.all_inbox,
            text: 'Tank Cabinet',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.tank),
          ),
          createDrawerBodyItem(
            icon: Icons.workspaces_outline,
            text: 'Biota',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.biota),
          ),
          createDrawerBodyItem(
            icon: Icons.event_note,
            text: 'Maintenance',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.maintenance),
          ),
          // createDrawerBodyItemExpand(
          //   icon: Icons.ac_unit,
          //   text: 'Test Expand',
          //   onTap: () =>
          //       Navigator.pushReplacementNamed(context, pageRoutes.haldua),
          // ),

          // Divider(),
          // createDrawerBodyItem(
          //   icon: Icons.notifications_active,
          //   text: 'Notifications',
          //   onTap: () => Navigator.pushReplacementNamed(
          //       context, pageRoutes.notification),
          // ),
          // createDrawerBodyItem(
          //   icon: Icons.contact_phone,
          //   text: 'Contact Info',
          //   onTap: () =>
          //       Navigator.pushReplacementNamed(context, pageRoutes.contact),
          // ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                 padding: EdgeInsets.all(50),
                  child: Text('App version 1.0.0')
              ),
              RaisedButton.icon(
              onPressed: () => showDialog(
                  useSafeArea: true,
                context: context,
                builder: (BuildContext context) => alertDialogModifikasi(context)),

              icon: Icon(Icons.logout),
              label: Text("Sign Out")
              ),

            ],
          )
          // ListTile(
          //   title: Text('App version 1.0.0'),
          //   onTap: () {},
          // ),
        ],
      ),
    );
  }
}

Widget createDrawerHeader() {
  GetStorage storage = GetStorage();

  return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fill, image: AssetImage('assets/images/headerbg.png'))),
      child: Stack(children: <Widget>[
        Positioned(
            bottom: 12.0,
            left: 16.0,
            child: Text("Hai ${storage.read("akun")}",
                style: TextStyle(
                    color: Colors.lightBlueAccent,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500))),
      ]));
}

Widget createDrawerBodyItem(
    {IconData? icon, String? text, GestureTapCallback? onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text!),
        )
      ],
    ),
    onTap: onTap,
  );
}

Widget createDrawerBodyItemExpand(
    {IconData? icon, String? text, GestureTapCallback? onTap}) {
  return ExpansionTile(
    title: Row(
      children: <Widget>[
        Icon(icon),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text!),
        )
      ],
    ),
    children: <Widget>[
      ListTile(
        title: Row(
          children: <Widget>[
            Icon(icon),
            Padding(
              padding: EdgeInsets.only(left: 8.0),
              child: Text('halaman selanjutnya'),
            )
          ],
        ),
        onTap: onTap,
      ),
    ],
  );
}
Widget alertDialogModifikasi(BuildContext context) {
  return AlertDialog(

    content: Container(
      child: Text("Keluar Aplikasi?"),
    ),
    title: Text("Warning !!"),
    actions: [
      RaisedButton(
          color: Colors.green,
          child: Text("Ya"),
          onPressed: () {
            //Navigator.of(context).pop();
            GetStorage pref = GetStorage();
            pref.erase();
            Navigator.pushReplacementNamed(context, "/");
          }),
      RaisedButton(
          color: Colors.red,
          child: Text("Tidak"),
          onPressed: () {
            Navigator.of(context).pop();
          }),
    ],
  );
}